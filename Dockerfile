FROM archlinux:base-devel-20220410.0.52530
MAINTAINER Michal Sotolar <michal@sotolar.com>

RUN pacman -Suyy --needed --noconfirm --noprogressbar base base-devel git \
	&& useradd -m -U build \
	&& echo "build ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/build
